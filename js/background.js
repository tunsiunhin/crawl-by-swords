var today = (new Date()).toLocaleDateString('en-GB');
var badge = window.localStorage.getItem(today);
if(!badge)
	window.localStorage.setItem(today, 0);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.badge == true){
      let setBadge = parseInt(badge)+parseInt(request.count);   
      chrome.browserAction.setBadgeText({text: setBadge.toString()});
      window.localStorage.setItem(today, setBadge);
    }
 });